# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 13:17:17 2019

@author: Thomas van der Linden, t.j.m.vanderlinden@uu.nl
"""

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from shapely.geometry import LineString

# Download the ETOPO Bedrock grid
etopo = Dataset('../data/raw/ETOPO/ETOPO1_Bed_g_gmt4.grd', 'r')

# Provide latlon coordinates of the elements to make profiles of
# For proper combination of multiple profiles use same direction with respect
# to plate motion
rifts = {
#        'ear_w': np.asarray([(-8.3, 30.6), (-7.2, 30.5), (-6., 29.6), (-4.7, 29.4), (-2.5, 29.0), (-0.3, 29.8), (1.6, 30.9)]), 
#        'ear_e': np.asarray([(3.9,34.5), (1.9,35.5),(0.5,35.8),(-1.4,36.4),(-3.4,36.4),(-5.1,36.1),(-7.2,35.8),(-8.3,34.3)]), 
        'ear': np.asarray([(3.87,33.06), (-8.96,33.51)]),
#        'galapagos': np.asarray([(2.1,-100.3),(2.2,-85.1)]),
#        'woodlark rift s': np.asarray([(-10.6,156.2),(-10.9,155.4),(-11.1,154.6),(-11.1,153.6),(-10.7,152.8),(-10.3,151.9)]), 
#        'red sea rift w': np.asarray([(12.6,43.0),(14.5,41.2),(17.0,38.8),(18.8,37.3),(22.,36.6),(23.9,35.5),(26.4,34.4)]), 
        'red sea': np.asarray([(27.93,34.11),(12.5,43.4)]),
        'california': np.asarray([(33.9,-116.5),(24.0,-108.9)]),
        'baikal': np.asarray([(55.9,110.),(53.1,107.9),(51.6,103.9)]),
        'corinth': np.asarray([(38.4,21.9),(38.0,23.1)]),
        'taupo': np.asarray([(-39.26,175.55),(-38.0,176.8)])
        }

arcs = {
#        'okinawa': np.asarray([(24.5,122), (24.6,124.3), (24.8,125), (25,125.6), (25.8,126.8), (26.8,128.1), (28.2,129.3), (30.9,131.1), (32.8,131.4)]), 
#        'greece': [(33.,22.), (38.,25.), 'NS'], 
#        'andaman': np.asarray([(14.3,93.4), (11.9,92.6), (10.,92.6), (8.4,93.2), (6.6,93.7)]), 
#        'scotia': np.asarray([(-60.0, -28.0), (-59.5, -27.2), (-59.0, -26.6), (-58.4, -26.4), (-57.7, -26.4), (-57.0, -26.6), (-56.7, -27.0), (-56.3, -27.5), (-55.9, -28.3)]), 
#        'mariana': np.asarray([(12.6,144.2), (14.1,145.1), (16.0,146.0), (18.2,145.7), (20.1,145.4), (21.5,144.4), (22.6,143.1)]),
        }

# Specify which profiles to plot
margin_types = [rifts, arcs]

# Specify spacing and length of profile lines
spc = 0.1 #spacing
sect_len = 3 # length of profile lines

xs = etopo['x'][:]
ys = etopo['y'][:]
zs = etopo['z'][:]

etopo.close()

def latlon_to_index(latlon):
    
    idx_from = int((latlon[0][1] + 180) * 60)
    idx_to = int((latlon[1][1] + 180) * 60)
    idy_from = int((latlon[0][0] + 90) * 60)
    idy_to = int((latlon[1][0] + 90) * 60)
    
    return idx_from, idy_from, idx_to, idy_to

def latlon_to_lat_lon(latlon):
    lon_from = latlon[0][1]
    lon_to = latlon[1][1]
    lat_from = latlon[0][0]
    lat_to = latlon[1][0]
    
    return lon_from, lat_from, lon_to, lat_to

mean_profiles_arcs = pd.DataFrame()
mean_profiles_rifts = pd.DataFrame()

for margin_type in margin_types:
    for section in margin_type:
        
        box = [(np.min(margin_type[section][:,0])-5,
                np.min(margin_type[section][:,1])-5),
               (np.max(margin_type[section][:,0]+5), 
                np.max(margin_type[section][:,1]+5))]
            
        idx_from, idy_from, idx_to, idy_to = latlon_to_index(box)
        lon_from, lat_from, lon_to, lat_to = latlon_to_lat_lon(box)
        
        line = LineString(margin_type[section])
        line_id = np.asarray(list(zip((margin_type[section][:,0] + 180) * 60, (margin_type[section][:,1] + 90) * 60)))
        
        n_prof = int(line.length/spc)
        
        prof_lines = []
        prof_lines_latlon = []
        
        for prof in range(1, n_prof+1):
            # Get the start, mid and end points for this segment
            seg_st = line.interpolate((prof-1)*spc)
            seg_mid = line.interpolate((prof-0.5)*spc)
            seg_end = line.interpolate(prof*spc)
        
            # Get a displacement vector for this segment
            vec = np.array([[seg_end.x - seg_st.x,], [seg_end.y - seg_st.y,]])
        
            # Rotate the vector 90 deg clockwise and 90 deg counter clockwise
            rot_anti = np.array([[0, -1], [1, 0]])
            rot_clock = np.array([[0, 1], [-1, 0]])
            vec_anti = np.dot(rot_anti, vec)
            vec_clock = np.dot(rot_clock, vec)
        
            # Normalise the perpendicular vectors
            len_anti = ((vec_anti**2).sum())**0.5
            vec_anti = vec_anti/len_anti
            len_clock = ((vec_clock**2).sum())**0.5
            vec_clock = vec_clock/len_clock
        
            # Scale them up to the profile length
            vec_anti = vec_anti*sect_len
            vec_clock = vec_clock*sect_len
        
            # Calculate displacements from midpoint
            prof_st = (seg_mid.x + float(vec_anti[0]), seg_mid.y + float(vec_anti[1]))
            prof_end = (seg_mid.x + float(vec_clock[0]), seg_mid.y + float(vec_clock[1]))
            prof_line = LineString(np.linspace(prof_st, prof_end, num=100))
            
            prof_lines_latlon.append((prof_st, prof_end))
            
            prof_st_id = ((prof_st[0]+90)*60, (prof_st[1]+180)*60)
            prof_end_id = ((prof_end[0]+90)*60, (prof_end[1]+180)*60)
            prof_line_id = (np.linspace(prof_st_id, prof_end_id, num=100)).astype(int)
            
            prof_lines.append(prof_line_id)
        
        prof_lines_latlon = np.asarray(prof_lines_latlon)
        
        profiles = pd.DataFrame(columns = ['dist', 'height'])
        dists = []
        heights = []
        
        for prof_line in prof_lines:
           
            start = prof_line[0]
            
            for coordinate in prof_line:
                heights.append(zs[coordinate[0]][coordinate[1]])
                
                dist = 5*(int((np.sqrt((coordinate[0] - start[0])**2 + (coordinate[1] - start[1])**2)/5).round()))
                dists.append(dist)
                    
        profiles['dist'] = pd.Series(dists) - np.mean(dists)
        profiles['height'] = pd.Series(heights)
        
        profile_min = profiles.groupby(by='dist').min()
        profile_mean = profiles.groupby(by='dist').mean()    
        profile_max = profiles.groupby(by='dist').max()    
        profile_mean['section'] = section
        
        if section in arcs:
            mean_profiles_arcs = mean_profiles_arcs.append(profile_mean)
        if section in rifts:
            mean_profiles_rifts = mean_profiles_rifts.append(profile_mean)
        
        plt.subplot(211) 
        plt.plot(profile_mean.index, profile_mean['height'])
        plt.plot(profile_min.index, profile_min['height'])
        plt.plot(profile_max.index, profile_max['height'])
        plt.title('%s, %s profile lines' %(section, len(prof_lines)))
        
        plt.subplot(212)    
        plt.pcolormesh(xs[idx_from-100:idx_to+100], ys[idy_from-100:idy_to+100], zs[idy_from-100:idy_to+100,idx_from-100:idx_to+100])
        plt.plot(margin_type[section][:,1], margin_type[section][:,0], c='white')
        
        for i in range(0,len(prof_lines_latlon),10):
            X = prof_lines_latlon[i][0][1]
            Y = prof_lines_latlon[i][0][0]
            U = prof_lines_latlon[i][1][1] - prof_lines_latlon[i][0][1]
            V = prof_lines_latlon[i][1][0] - prof_lines_latlon[i][0][0]
            plt.arrow(X, Y, U, V, color='black', head_width=0.8)
        
        plt.gca().set_aspect('equal', adjustable='box')
        plt.savefig('../results/figures/swath %s.png' %section, bbox_inches='tight', dpi=600)
        plt.show()
    
    if len(mean_profiles_arcs) > 0:    
        for section in mean_profiles_arcs['section'].unique():
            X = mean_profiles_arcs[mean_profiles_arcs['section']==section].index
            Y = mean_profiles_arcs[mean_profiles_arcs['section']==section]['height']
            plt.plot(X, Y, label=section)
        plt.title('arcs')
        plt.legend()
        plt.savefig('../results/figures/swath arcs.png', bbox_inches='tight', dpi=600)
        plt.show()
    
    if len(mean_profiles_rifts) > 0:        
        for section in mean_profiles_rifts['section'].unique():
            X = mean_profiles_rifts[mean_profiles_rifts['section']==section].index
            Y = mean_profiles_rifts[mean_profiles_rifts['section']==section]['height']
            plt.plot(X, Y, label=section)
        plt.title('rifts')
        plt.legend()
        plt.savefig('../results/figures/swath rifts.png', bbox_inches='tight', dpi=600)
        plt.show()